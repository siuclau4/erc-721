import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { network, deployments, ethers } from "hardhat";
import { developmentChains } from "../../helper-hardhat-config";
import { StanNFT } from "../../typechain";

!developmentChains.includes(network.name)
  ? describe.skip
  : describe("StanNFT Unit Tests", () => {
      let stanNft: StanNFT;
      let deployer: SignerWithAddress;
      let otherUser: SignerWithAddress;

      beforeEach(async () => {
        [deployer, otherUser] = await ethers.getSigners();
        await deployments.fixture(["all"]);
        stanNft = await ethers.getContract("StanNFT");
      });

      it("can change pause status by owner", async () => {
        await stanNft.connect(deployer).setPaused(false);
        const isPause = await stanNft.paused();

        expect(isPause).to.be.equal(false);
      });

      it("is not allow change pause status by other users", async () => {
        await expect(stanNft.connect(otherUser).setPaused(false)).to.be.revertedWith(
          "Ownable: caller is not the owner"
        );
      });

      it("can change reveal status by owner", async () => {
        await stanNft.connect(deployer).setRevealed(true);
        const isRevealed = await stanNft.revealed();

        expect(isRevealed).to.be.equal(true);
      });

      it("is not allow change reveal status by other users", async () => {
        await expect(stanNft.connect(otherUser).setRevealed(true)).to.be.revertedWith(
          "Ownable: caller is not the owner"
        );
      });

      it("can change prefix by owner", async () => {
        const newUriPrefix = "http://testing.com/";
        await stanNft.connect(deployer).setUriPrefix(newUriPrefix);
        const currentUriPrefix = await stanNft.uriPrefix();

        expect(currentUriPrefix).to.be.equal(newUriPrefix);
      });

      it("is not allow change prefix by other users", async () => {
        const newUriPrefix = "http://testing.com/";
        await expect(stanNft.connect(otherUser).setUriPrefix(newUriPrefix)).to.be.revertedWith(
          "Ownable: caller is not the owner"
        );
      });

      it("can change suffix by owner", async () => {
        const newUriSuffix = ".txt";
        await stanNft.connect(deployer).setUriSuffix(newUriSuffix);
        const currentUriSuffix = await stanNft.uriSuffix();

        expect(currentUriSuffix).to.be.equal(newUriSuffix);
      });

      it("is not allow change suffix by other users", async () => {
        const newUriSuffix = ".txt";
        await expect(stanNft.connect(otherUser).setUriSuffix(newUriSuffix)).to.be.revertedWith(
          "Ownable: caller is not the owner"
        );
      });

      it("can change hidden metadata uri by owner", async () => {
        const newHiddenMetadataUri = "ipfs://__CID__/new-hidden.json";
        await stanNft.connect(deployer).setHiddenMetadataUri(newHiddenMetadataUri);
        const currentHiddenMetadataUri = await stanNft.hiddenMetadataUri();

        expect(currentHiddenMetadataUri).to.be.equal(newHiddenMetadataUri);
      });

      it("is not allow change hidden metadata uri by other users", async () => {
        const newHiddenMetadataUri = "ipfs://__CID__/new-hidden.json";
        await expect(
          stanNft.connect(otherUser).setHiddenMetadataUri(newHiddenMetadataUri)
        ).to.be.revertedWith("Ownable: caller is not the owner");
      });

      it("can let minters to mint", async () => {
        const minter = ethers.Wallet.createRandom().connect(ethers.provider);

        await deployer.sendTransaction({
          to: await minter.getAddress(),
          value: ethers.utils.parseUnits("1", "ether"),
        });

        await stanNft.connect(deployer).setPaused(false);

        const mintAmount = 5;
        const payableAmount = 5 * 0.05;
        const tx = await stanNft.connect(minter).mint(mintAmount, {
          value: ethers.utils.parseEther(payableAmount.toString()),
        });
        await tx.wait();

        const minterBalance = await stanNft.balanceOf(minter.address);

        expect(minterBalance).to.be.equal(mintAmount);
      });

      it("is not allow minters to mint more than 5 per tx", async () => {
        const minter = ethers.Wallet.createRandom().connect(ethers.provider);

        await deployer.sendTransaction({
          to: await minter.getAddress(),
          value: ethers.utils.parseUnits("1", "ether"),
        });

        await stanNft.connect(deployer).setPaused(false);

        const mintAmount = 6; // more than 5
        const payableAmount = mintAmount * 0.05;

        await expect(
          stanNft.connect(minter).mint(mintAmount, {
            value: ethers.utils.parseEther(payableAmount.toString()),
          })
        ).to.be.revertedWith("Invalid mint amount!");
      });

      it("is not allow minters to mint if pay amount is not enough", async () => {
        const minter = ethers.Wallet.createRandom().connect(ethers.provider);

        await deployer.sendTransaction({
          to: await minter.getAddress(),
          value: ethers.utils.parseUnits("1.0", "ether"),
        });

        await stanNft.connect(deployer).setPaused(false);

        const mintAmount = 4; // equal or less than 5
        const payableAmount = mintAmount * 0.04; // price is 0.05, 0.04 is not enough

        await expect(
          stanNft.connect(minter).mint(mintAmount, {
            value: ethers.utils.parseEther(payableAmount.toString()),
          })
        ).to.be.revertedWith("Insufficient funds!");
      });

      it("can show correct token URL after reverling", async () => {
        // 1.  minting
        const minter = ethers.Wallet.createRandom().connect(ethers.provider);

        await deployer.sendTransaction({
          to: await minter.getAddress(),
          value: ethers.utils.parseUnits("1", "ether"),
        });

        await stanNft.connect(deployer).setPaused(false);

        const mintAmount = 5;
        const payableAmount = 5 * 0.05;
        const tx = await stanNft.connect(minter).mint(mintAmount, {
          value: ethers.utils.parseEther(payableAmount.toString()),
        });
        await tx.wait();

        // 2. reveal
        await stanNft.connect(deployer).setRevealed(true);

        // 3. assert
        const prefix = await stanNft.uriPrefix();
        const suffix = await stanNft.uriSuffix();

        await expect(await stanNft.tokenURI(0)).to.be.equal(`${prefix}0${suffix}`);
      });

      it("can let minters to mint ALL 1000 NFTs", async () => {
        const minter = ethers.Wallet.createRandom().connect(ethers.provider);

        await deployer.sendTransaction({
          to: await minter.getAddress(),
          value: ethers.utils.parseUnits("800", "ether"),
        });

        await stanNft.connect(deployer).setPaused(false);

        let mintedAmount: number = 0;

        while (mintedAmount < 1000) {
          const mintAmount = 5;
          const payableAmount = mintAmount * 0.05;
          const tx = await stanNft.connect(minter).mint(mintAmount, {
            value: ethers.utils.parseEther(payableAmount.toString()),
          });
          await tx.wait();

          mintedAmount += mintAmount;
        }

        const minterBalance = await stanNft.balanceOf(minter.address);

        expect(minterBalance).to.be.equal(1000);

        // 4. cannot mint exceed 1000
        await expect(
          stanNft.connect(minter).mint(1, {
            value: ethers.utils.parseEther((0.05).toString()),
          })
        ).to.be.rejectedWith("Max supply exceeded!");
      });
    });
