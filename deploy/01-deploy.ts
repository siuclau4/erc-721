import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import verify from "../utils/verify";
import { developmentChains } from "../helper-hardhat-config";

const deployFunction: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { getNamedAccounts, deployments, network } = hre;
  const { deploy, log } = deployments;
  const { deployer } = await getNamedAccounts();

  log("Deploy StanNFT");

  const stanNft = await deploy("StanNFT", {
    from: deployer,
    args: [],
    log: true,
  });

  log("StanNFT deployed at " + stanNft.address);
};

deployFunction.tags = ["all", "nft"];

export default deployFunction;
