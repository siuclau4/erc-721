// SPDX-License-Identifier: MIT

pragma solidity ^0.8.4;

import "../StanNFT.sol";

contract StanNFTFuzzingTest is StanNFT {
  function echidna_test_max_total_supply() public view returns (bool) {
    return totalSupply() <= MAX_SUPPLY;
  }
}
