# Installation

Install hardhat

```
npm install --save-dev hardhat
```

Install dependencies

```
npm i
```

# Run unit test before deployment

```
npx hardhat test
```


# Step 1: Deploy contract

```
npx hardhat deploy --tags all --network <ETH_NETWORK>
```

# Step 2: Verify contract on Etherscan

```
npx hardhat verify --network <ETH_NETWORK> <CONTRACT_ADDRESS>
```