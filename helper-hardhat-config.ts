export interface networkConfigItem {
  name?: string
}

export interface networkConfigInfo {
  [key: number]: networkConfigItem
}

export const networkConfig: networkConfigInfo = {
  31337: {
    name: "hardhat",
  },
  4: {
    name: "rinkeby",
  },
}

export const developmentChains = ["hardhat"]
